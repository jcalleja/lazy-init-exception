# README #

This is a repo to share code of submitted SO question: [http://stackoverflow.com/questions/24587060/using-spring-roo-project-and-getting-org-hibernate-lazyinitializationexception/24604751#24604751](http://stackoverflow.com/questions/24587060/using-spring-roo-project-and-getting-org-hibernate-lazyinitializationexception/24604751#24604751)

To get started:

git clone https://jcalleja@bitbucket.org/jcalleja/lazy-init-exception.git

cd in dir. 

If using Eclipse:

1. mvn eclipse:eclipse
2. Import as existing eclipse project

Run main in src/main/java/com/tmp
