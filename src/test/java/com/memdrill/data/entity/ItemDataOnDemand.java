package com.memdrill.data.entity;

import org.springframework.roo.addon.dod.RooDataOnDemand;

import com.memdrill.data.util.LobString;

@RooDataOnDemand(entity = Item.class)
public class ItemDataOnDemand {

	public void setContent(Item obj, int index) {
        LobString content = new LobString("this is the content of the item");
        obj.setContent(content);
    }
}
