package com.tmp;

import java.util.List;

import javax.persistence.EntityManager;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.transaction.annotation.Transactional;

import com.memdrill.data.entity.Item;
import com.memdrill.data.util.LobString;

public class Main {

	@Transactional
	public static void main(String[] args) {
		ClassPathXmlApplicationContext appContext = new ClassPathXmlApplicationContext(
				"classpath*:META-INF/spring/applicationContext*.xml");
		System.out.println("Defined beans:");
		String[] beanNames = appContext.getBeanDefinitionNames();
		for(String beanName : beanNames) {
			System.out.println("> " + beanName);
		}
			
		main3();
		

		appContext.close();
	}
	
	
	private static void main3() {
		System.out.println("------------------- main3 -------------------");
		Item item = new Item();
		item.setItemId("abc123");
		item.setContent(new LobString("this is the lob data"));

		item.persist();

		System.out.println("persist - OK");
		
		List<Item> items = Item.findAllItems();
		
		Item i = items.get(0);
		
		System.out.println("i.getId() = " + i.getId());
		System.out.println("i.getItemId() = " + i.getItemId());
//		System.out.println("i.getContent() = " + i.getContent());
		System.out.println("i.loadContent() = " + i.loadContent());
		System.out.println("i.getContent().getId() = " + i.getContent().getId());
//		System.out.println("i.getContent().getData() = " + i.getContent().getData());
		
	}
	
	private static void main2() {
		System.out.println("------------------- main2 -------------------");
//		EntityManager em = Item.entityManager();
//		Not allowed to create transaction on shared EntityManager - use Spring transactions instead
//		EntityTransaction t = em.getTransaction();
//		System.out.println("transaction = " + t);
//		System.out.println("t.isActive() = " + t.isActive());
		
		Item item = new Item();
		item.setItemId("abc123");
		LobString content = new LobString();
		content.setData("this is the log data");
		item.setContent(content);

		item.persist();
//		em.persist(item);
//		em.flush();
//        this.entityManager.persist(this);

		System.out.println("persist - OK - in Main of memdrill-data not data-use");
		
//        List<Item> items = em.createQuery("SELECT o FROM Item o", Item.class).getResultList();
//        List<Item> items = Item.entityManager().createQuery("SELECT o FROM Item o", Item.class).getResultList();
		List<Item> items = Item.findAllItems();
//		List<Item> items = Item.findAllItemsWithRefs();
//		EntityManager em = Item.entityManager();
//        List<Item> items = em.createQuery("SELECT o FROM Item o JOIN FETCH o.content", Item.class).getResultList();
//		select employee, employee.dept.name from Employee employee
//        List<Item> items = em.createQuery("SELECT o, o.content.id FROM Item o", Item.class).getResultList();
//        List<Item> items = em.createQuery("SELECT o.content.id FROM Item o", Item.class).getResultList();
		
		Item i = items.get(0);
		
		System.out.println("i.getId() = " + i.getId());
		System.out.println("i.getItemId() = " + i.getItemId());
//		System.out.println("i.getContent() = " + i.getContent());
		System.out.println("i.getContent().getId() = " + i.getContent().getId());
		System.out.println("i.getContent().getData() = " + i.getContent().getData());
		
	}

}
