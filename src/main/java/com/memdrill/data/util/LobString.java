package com.memdrill.data.util;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;

@RooJavaBean
@RooEntity
public class LobString {
	
	@Lob
	@Basic(fetch=FetchType.LAZY)
	private String data;

	public LobString() {
		super();
		System.out.println("in LobString default constructor");
	}
	
	public LobString(String data) {
		this.data = data;
		System.out.println("in LobString (String data) constructor");
	}

	public String getData() {
        return this.data;
    }

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
//	@Access(AccessType.PROPERTY)
    private Long id;

	public Long getId() {
        return this.id;
    }
	
}
