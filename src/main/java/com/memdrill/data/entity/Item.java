package com.memdrill.data.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;
import org.springframework.transaction.annotation.Transactional;

import com.memdrill.data.util.LobString;

@RooJavaBean
@RooToString
@RooEntity
public class Item {

	private final static org.slf4j.Logger LOG = org.slf4j.LoggerFactory
			.getLogger(Item.class);

	@NotNull
	@Column(unique = true)
	@Size(min = 1, max = 200)
	private String itemId;

	@NotNull
	@Lob
	@OneToOne(cascade = javax.persistence.CascadeType.ALL, fetch = FetchType.LAZY)
	private LobString content;

	// @Query("SELECT p FROM Person p JOIN FETCH p.roles WHERE p.id = (:id)")
	// public Person findByIdAndFetchRolesEagerly(@Param("id") Long id);
	public static List<Item> findAllItemsWithRefs() {
		List<Item> items = entityManager().createQuery(
				"SELECT o FROM Item o JOIN FETCH o.content", Item.class)
				.getResultList();
		// TypedQuery<Item> query =
		// entityManager().createQuery("SELECT o FROM Item o", Item.class);

		for (Item item : items) {
			System.out.println("item.getContent().getId() = "
					+ item.getContent().getId());
		}
		return items;
	}

	// @OneToOne(fetch=FetchType.LAZY, optional=false)
	public LobString getContent() {
		return this.content;
	}

	@Transactional(readOnly = true)
	public LobString loadContent() {
		System.out.println("in loadContent....");
		return getContent();
	}
}
